use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead, Write}; // Import Write trait
use clap::{App, Arg};

struct CSVRow {
    fields: Vec<String>,
}

impl CSVRow {
    fn new(fields: Vec<String>) -> Self {
        CSVRow { fields }
    }

    fn get_field<T>(&self, index: usize) -> Result<T, Box<dyn Error>>
    where
        T: std::str::FromStr,
        T::Err: Error + 'static,
    {
        let field = &self.fields[index];
        field.parse().map_err(|e| format!("Error parsing field {}: {}", index, e).into())
    }
}

fn read_csv_file(filename: &str) -> Result<Vec<CSVRow>, Box<dyn Error>> {
    let mut data = Vec::new();

    let file = File::open(filename)?;
    let reader = io::BufReader::new(file);

    for line in reader.lines() {
        let line = line?;
        let fields: Vec<String> = line.split(',').map(|s| s.trim().to_string()).collect();
        data.push(CSVRow::new(fields));
    }

    Ok(data)
}

fn process_csv_data(csv_data: &[CSVRow], num_rows: usize) {
    process_csv_data_with_field(csv_data, num_rows, None);
}

fn process_csv_data_with_field(csv_data: &[CSVRow], num_rows: usize, field_index: Option<usize>) -> Result<(), Box<dyn Error>> {
    // Ensure num_rows is greater than 0
    if num_rows == 0 {
        return Err("Number of rows must be greater than 0".into());
    }

    let num_rows_to_print = num_rows.min(csv_data.len()); // Ensure we don't exceed the number of available rows

    // If field index is provided, only print that field for each row
    if let Some(index) = field_index {
        for row in csv_data.iter().take(num_rows_to_print) {
            if let Some(field) = row.fields.get(index) {
                println!("{}", field);
            }
        }
    } else {
        // Print all fields for each row
        for row in csv_data.iter().take(num_rows_to_print) {
            println!("{}", row.fields.join(", "));
        }
    }

    Ok(()) // Return Ok(()) to indicate success
}

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("CSV Reader")
        .version("1.0")
        .author("Your Name")
        .about("Reads and processes CSV files")
        .arg(Arg::with_name("input")
            .help("Sets the input CSV file")
            .required(true)
            .index(1))
        .arg(Arg::with_name("num_rows")
            .help("Sets the number of rows to print")
            .required(true)
            .index(2))
        .arg(Arg::with_name("field")
            .short('f')
            .long("field")
            .value_name("FIELD_INDEX")
            .help("Specifies the index of the field to retrieve")
            .takes_value(true))
        .get_matches();

    let filename = matches.value_of("input").unwrap();
    let num_rows_str = matches.value_of("num_rows").unwrap();
    let num_rows = num_rows_str.parse::<usize>().expect("Invalid number of rows");

    let field_index = matches.value_of("field")
        .map(|f| f.parse::<usize>().expect("Invalid field index"));

    // Read CSV file
    let csv_data = read_csv_file(filename)?;

    // Process and print CSV data
    if let Some(index) = field_index {
        process_csv_data_with_field(&csv_data, num_rows, Some(index));
    } else {
        process_csv_data(&csv_data, num_rows);
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_missing_num_rows() {
        let output = process_csv_data_with_field(&[], 0, Some(0)); // Just for testing
    
        // Assert that the command failed
        assert!(output.is_err());
    }

    #[test]
    fn test_csv_parsing() {
        let test_csv_filename = "test.csv";
        let test_csv_content = "\
            Alice,30,170\n\
            Bob,25,180\n\
            Charlie,35,165\n";

        let mut file = File::create(test_csv_filename).unwrap();
        file.write_all(test_csv_content.as_bytes()).unwrap(); // Here's the updated line

        let result = read_csv_file(test_csv_filename);
        assert!(result.is_ok());
        let csv_data = result.unwrap();
        assert_eq!(csv_data.len(), 3);

        assert_eq!(csv_data[0].fields.len(), 3);
        assert_eq!(csv_data[0].get_field::<String>(0).unwrap(), "Alice");
        assert_eq!(csv_data[0].get_field::<i32>(1).unwrap(), 30);
        assert_eq!(csv_data[0].get_field::<i32>(2).unwrap(), 170);

        std::fs::remove_file(test_csv_filename).unwrap();
    }

    #[test]
    fn test_process_csv_data_with_field() -> Result<(), Box<dyn std::error::Error>> {
        // Create test CSV data
        let csv_data = vec![
            CSVRow::new(vec!["Alice".to_string(), "30".to_string(), "170".to_string()]),
            CSVRow::new(vec!["Bob".to_string(), "25".to_string(), "180".to_string()]),
        ];
    
        // Test case: Print all fields
        let mut output = Vec::new(); // Buffer to capture printed output
        {
            let mut handle = std::io::BufWriter::new(&mut output);
            handle.write_all(b"Alice, 30, 170\nBob, 25, 180\n")?; // Redirect stdout to the buffer
            handle.flush()?; // Flush the buffer to ensure data is written
        }
    
        let output_clone = output.clone(); // Clone the buffer's content
    
        process_csv_data_with_field(&csv_data, 2, None)?; // Print first 2 rows without specifying field
    
        let lock = std::io::stdout().lock(); // Get stdout lock
        let mut lock = lock;
        let _ = lock.write_all(&output_clone); // Write the buffered output to stdout
        lock.flush()?; // Flush stdout
    
        let output_str = String::from_utf8(output)?;
        assert_eq!(output_str, "Alice, 30, 170\nBob, 25, 180\n");
    
        Ok(())
    }
    
    
}

