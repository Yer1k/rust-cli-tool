# Rust CLI Tool to process a CSV file

[![CI/CD status](https://gitlab.com/Yer1k/rust-cli-tool/badges/main/pipeline.svg)](https://gitlab.com/Yer1k/rust-cli-tool/-/commits/main)


This is a simple CLI tool written in Rust to process a CSV file. The tool reads a CSV file and prints the content to the console. It allow user to specify how many records to print.

## Features
- Read a CSV file and print its content to the console

- Allow user to specify how many records to print

```bash
$ cargo run -- -f <file> -n <number>
```

- `<file>`: Path to the CSV file
- `<number>`: Number of records to print

## Example Screenshots

### Run the tool

```bash
$ cargo run Prediction\ Model\ Quality\ Check_Verification\ of\ Predicted\ Images.csv 5
```

![Example](screenshots/run_example.png)

### Sample CSV File Content

![CSV File](screenshots/csv_example.png)

## Unit Tests

```bash
$ cargo test
```

![Unit Tests](screenshots/testing.png)

### Test cases

1. Test if the CSV file is read correctly
1. Test if the number of records to print is valid
1. Test processing of the CSV file with field filtering

![Test Cases](screenshots/tests.png)

